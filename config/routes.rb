Rails.application.routes.draw do
  resources :references
  resources :skills_and_interests
  resources :honors_and_awards
  resources :work_experiences
  devise_for :users
  root to: "dashboard#index"

  get 'dashboard/index'

  get 'dashboard/page1'

  get 'dashboard/page2'

  get 'dashboard/download'

  get 'work_experiences/index'

  get 'honors_and_awards/index'

  get 'skills_and_interests/index'

  get 'references/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
