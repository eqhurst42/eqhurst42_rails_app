$('document').ready(function() {

    if ($('#experienceList').length) {
        var myExpList = document.getElementById("experienceList").getElementsByTagName("tr");
        for (i = 0; i < myExpList.length; i++) {
            myExpList[i].addEventListener("click", activateExpItem);
        }
    }

    function activateExpItem() {
        var company = this.childNodes[1].innerHTML;
        var position = this.childNodes[7].innerHTML;
        document.getElementById("myModalLabel").innerHTML = company;
        document.getElementById("myModalBody").innerHTML = position;

    }

    if ($('#honorList').length) {
        var myHonList = document.getElementById("honorList").getElementsByTagName("tr");
        for (i = 0; i < myHonList.length; i++) {
            myHonList[i].addEventListener("click", activateHonItem);
        }
    }

    function activateHonItem() {
        var name = this.childNodes[3].innerHTML;
        var description = this.childNodes[5].innerHTML;
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = description;

    }

    if ($('#skillList').length) {
        var mySkillList = document.getElementById("skillList").getElementsByTagName("tr");
        for (i = 0; i < mySkillList.length; i++) {
            mySkillList[i].addEventListener("click", activateSkillItem);
        }
    }

    function activateSkillItem() {
        var name = this.childNodes[1].innerHTML;
        var description = this.childNodes[3].innerHTML;
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = description;

    }

    if ($('#refList').length) {
        var myRefList = document.getElementById("refList").getElementsByTagName("tr");
        for (i = 0; i < myRefList.length; i++) {
            myRefList[i].addEventListener("click", activateRefItem);
        }
    }

    function activateRefItem() {
        var name = this.childNodes[1].innerHTML;
        var info = this.childNodes[3].innerHTML;
        document.getElementById("myModalLabel").innerHTML = name;
        document.getElementById("myModalBody").innerHTML = info;

    }

});
