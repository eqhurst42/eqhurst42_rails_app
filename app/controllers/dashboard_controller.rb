class DashboardController < ApplicationController
  def index
    if user_signed_in?
      @references = Reference.where("owner = ?", current_user.id)
      @skills_and_interests = SkillsAndInterest.where("owner = ?", current_user.id)
      @work_experience = WorkExperience.where("owner = ?", current_user.id)
      @honors_and_awards = HonorsAndAward.where("owner = ?", current_user.id)
    end
  end

  def download
    user = User.find(current_user.id)
    @references = Reference.where("owner = ?", current_user.id)
    @skills_and_interests = SkillsAndInterest.where("owner = ?", current_user.id)
    @work_experience = WorkExperience.where("owner = ?", current_user.id)
    @honors_and_awards = HonorsAndAward.where("owner = ?", current_user.id)



        Caracal::Document.save '/public/Resume.docx' do |docx|
          docx.style do
            id 'Body'
            name 'body'
            font 'Segoe UI Light'
            size 24
          end
              
          # page 1
          docx.h1 do
            text user.first_name
            text ' '
            text user.last_name
          end

          docx.hr
          docx.p do
            text user.email
          end
          docx.hr

          docx.h2 'Work Experience'
          @work_experience.each do | experience |
            docx.p do
              style 'Body'
              text experience.name
              text ' ('
              text experience.start_date
              text ' - '
              text experience.end_date
              text ')'
              br
              text experience.description
            end
            docx.p
          end

          docx.p
          docx.h2 'Skills and Interests'
          @skills_and_interests.each do | skill |
            docx.p do
              style 'Body'
              text skill.skill_name
              br
              text skill.skill_description
            end
            docx.p
          end

          docx.p
          docx.h2 'Honors and Awards'
          @honors_and_awards.each do | honor |
            docx.p do
              style 'Body'
              text honor.honor_name
              text ' ('
              text honor.honor_date
              text ')'
              br
              text honor.honor_description
            end
            docx.p
          end

          docx.p
          docx.h2 'References'
          @references.each do | reference |
            docx.p do
              style 'Body'
              text reference.reference_name
              text ' '
              text reference.reference_contact_info
            end
            docx.p
          end









        end
        
        path = File.join(Rails.root, "public")
        send_file(File.join(path, "Resume.docx"))
  end

  def references
    @reference = Reference.where("owner = ?", current_user.id)
  end

end
